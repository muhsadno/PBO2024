package P02;

public class P02d {
    public static void main(String[] args) {
        Mahasiswa iman = new Mahasiswa();
        iman.nama = "Iman";
        iman.nim = "H0719999";
        iman.ipk = 3.15;

        // String data = iman.sayHi();

        Mahasiswa yuni = new Mahasiswa();
        yuni.nama = "Yuni";
        yuni.nim = "A000999";
        yuni.ipk = 3.99;

        // boolean cek = yuni.cekIPK();
        
        if (yuni.cekIPK()) {
            System.out.println("Selamat");
        }
        
    }    
}

class Mahasiswa {
    String nama;
    String nim;
    double ipk;

    void sayHi() {
        System.out.println("Hi "+ nama);
    }

    //metode cek ipk > 3.5 -> boolean
    boolean cekIPK() {
        if (ipk > 3.5) {
            return true;
        } else {
            return false;
        }
    }

}
