package P02;

import java.util.Scanner;

public class P02a {

    public static void main(String[] args) {
        int x;
        Scanner input = new Scanner(System.in);
        System.out.print("Input x = ");
        x = input.nextInt();
        System.out.println("x = "+x);
        
        String kampus;
        System.out.print("Input kampus = ");
        kampus = input.next();
        System.out.println("Kampus " + kampus);
        input.close();

    }    
}
