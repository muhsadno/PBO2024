package P03;

public class Lingkaran {

    double r;

    Lingkaran(double r) {
        this.r = r;
    }

    double luas() {
        return 3.1415 * r * r;
    }

    double keliling() {
        return 2 * 3.1415 * r;
    }
    
}
