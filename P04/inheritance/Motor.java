package inheritance;

public class Motor {

    int tahunrakit;

    public static void main(String[] args) {
        Kendaraan yamaha = new Kendaraan();
        yamaha.cetak();
    }

    void cetak() {
        System.out.println("Kendaraan dengan warna " + tahunrakit);
    }    
}
