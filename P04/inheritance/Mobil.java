package inheritance;

public class Mobil extends Kendaraan {

    int jumRoda;
    int kapasitas;
        
    public static void main(String[] args) {
        Mobil avanza = new Mobil();
        avanza.merk = "Avanza";
        avanza.warna = "Hitam";
        avanza.jumRoda = 4;
        avanza.cetak();
    }

    @Override
    void cetak() {
        System.out.println("Mobil " + this.merk + " berwarna " + this.warna);
    }

    void kapasitas() {
        System.out.println("Kendaraan ini berkapasitas " + this.kapasitas);
    }

    
}
