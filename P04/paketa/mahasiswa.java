package paketa;

public class mahasiswa {

    public String nama;
    String nim;
    protected int usia;
    private double ipk;
    
    // getter
    public double getIPK() {
        return ipk;
    }

    // setter
    public void setIPK(double ipk) {
        this.ipk = ipk;
    }

    public void sayHello() {
        System.out.println("Hello " + nama);
    }

    void cekIPK() {
        if (ipk > 3.00) {
            System.out.println("IPK " + nama + " di atas standar");
        } else {
            System.out.println("IPK " + nama + " di bawah standar");
        }
    }

}