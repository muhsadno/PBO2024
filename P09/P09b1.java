package P09;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.UIManager;
import java.awt.event.*;

public class P09b1 { //kelas PBO-B

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            System.out.println("error :" + e.getMessage());
        }

        JFrame jf = new JFrame("My App");
        jf.setSize(200, 200);

        JButton btnExit = new JButton("Exit");
        btnExit.setBounds(50, 50, 50, 50);
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            } 
        });
        jf.add(btnExit);

        jf.setLayout(null);
        jf.setVisible(true);
        
    }
    
}
