package P09;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class P09b {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            System.out.println("error :" + e.getMessage());
        }

        JFrame jf = new JFrame("My App");
        jf.setSize(300, 300);

        JTextField fnama = new JTextField();
        fnama.setBounds(10, 10, 100, 30);
        jf.add(fnama);

        JLabel hello = new JLabel();
        hello.setBounds(10,40,150,30);
        jf.add(hello);

        JButton btnHello = new JButton();
        btnHello.setText("Hello");
        btnHello.setBounds(180,50,80,30);
        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nama = fnama.getText();
                hello.setText("Hello " + nama);
                throw new UnsupportedOperationException("Unimplemented method 'actionPerformed'");
            }
            
        });
        jf.add(btnHello);

        JButton btnReset = new JButton("Reset");
        btnReset.setBounds(180,100,100,30);
        btnReset.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                hello.setText("");
                fnama.setText("");
                throw new UnsupportedOperationException("Unimplemented method 'actionPerformed'");
            }
            
        });
        jf.add(btnReset);

        JButton btnExit = new JButton();
        btnExit.setText("Exit");
        btnExit.setBounds(180, 180, 80, 30);
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
                throw new UnsupportedOperationException("Unimplemented method 'actionPerformed'");
            }
        });
        jf.add(btnExit);
        
        jf.setLayout(null);
        jf.setVisible(true);
    }
}
