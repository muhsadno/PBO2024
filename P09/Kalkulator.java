package P09;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class Kalkulator {

    public static void main(String[] args) {
        JFrame jf = new JFrame("My App");
        jf.setSize(200, 200);

        JTextField fieldA = new JTextField();
        fieldA.setBounds(5, 5, 80, 30);
        jf.add(fieldA);

        JTextField fieldB = new JTextField();
        fieldB.setBounds(5, 40, 80, 30);
        jf.add(fieldB);

        JTextField fieldHasil = new JTextField();
        fieldHasil.setBounds(5, 90, 80, 30);
        fieldHasil.setEditable(false);
        jf.add(fieldHasil);

        JButton btnBagi = new JButton(":");
        btnBagi.setBounds(90, 5, 50, 30);
        btnBagi.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                double A = Double.parseDouble(fieldA.getText());
                double B = Double.parseDouble(fieldB.getText());
                fieldHasil.setText(String.valueOf(A/B));
                throw new UnsupportedOperationException("Unimplemented method 'actionPerformed'");
            }
            
        });
        jf.add(btnBagi);

        jf.setLayout(null);
        jf.setVisible(true);
    }
    
}
