package P09;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class KalkulatorSederhana {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            System.out.println("error :" + e.getMessage());
        }


        JFrame jf = new JFrame("My App");
        jf.setSize(150, 200);

        JTextField tfA = new JTextField();
        tfA.setBounds(5, 5, 50, 30);
        jf.add(tfA);  
        
        JTextField tfB = new JTextField();
        tfB.setBounds(60, 5, 50, 30);
        jf.add(tfB);

        JTextField tfHasil = new JTextField();
        tfHasil.setBounds(5, 100, 50, 30);
        tfHasil.setEditable(false);
        jf.add(tfHasil);

        JButton btnTambah = new JButton("+");
        btnTambah.setBounds(5, 50, 50, 30);
        btnTambah.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String txtA = tfA.getText();
                String txtB = tfB.getText();
                double A = Double.parseDouble(txtA);
                double B = Double.parseDouble(txtB);
                double C = A + B;
                tfHasil.setText(""+C);
                throw new UnsupportedOperationException("Unimplemented method 'actionPerformed'");
            }
            
        });
        jf.add(btnTambah);

        
        jf.setLayout(null);
        jf.setVisible(true);
    }
    
}
