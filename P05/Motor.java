package P05;

class Motor extends Kendaraan {

    String pabrikan;

    void cetakMotor() {
        System.out.println("motor pabrikan " + pabrikan + " beroda " + jumRoda);
    }

    @Override
    void cetakJumRoda() {
        System.out.println("motor beroda " + jumRoda + " dirakit pada tahun " + tahunPembuatan);
    }

}
