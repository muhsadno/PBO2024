package P05;

class Kendaraan {

    String jenis;
    int jumRoda;
    int tahunPembuatan;

    Kendaraan() {
        System.out.println("-- ini kendaraan --");
    }

    Kendaraan(String jenis) {
        this.jenis = jenis;
        System.out.println("--- CHECK ---" + this.jenis);
    }

    void cetakJenis() {
        System.out.println("kendaraan berjenis " + jenis);
    }

    void cetakJumRoda() {
        System.out.println("kendaraan beroda " + jumRoda);
    }

}
