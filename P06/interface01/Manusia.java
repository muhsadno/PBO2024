package P06.interface01;

abstract class Manusia {

    String nama;
    int tinggiBadan;
    int beratBadan;
    String warnaKulit;

    void cetak() {
        System.out.println("Nama " + nama);
        System.out.println("tinggi " + tinggiBadan + " , berat " + beratBadan);
        System.out.println("warna kulit " + warnaKulit);
    }
    
}
