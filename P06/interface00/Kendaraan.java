package P06.interface00;

interface Kendaraan {

    void cetakJenis(String jenis);

    void cetakRoda(int roda);
    
}
