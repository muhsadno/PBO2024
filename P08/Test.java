import javax.swing.UIManager;

public class Test {

    public static void main(String[] args) {
        UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
        for (UIManager.LookAndFeelInfo look : looks) {
            System.out.println(look.getClassName());
        }

        // UI Theme , copy one of them
        // javax.swing.plaf.metal.MetalLookAndFeel
        // javax.swing.plaf.nimbus.NimbusLookAndFeel
        // com.sun.java.swing.plaf.motif.MotifLookAndFeel
        // com.sun.java.swing.plaf.windows.WindowsLookAndFeel
        // com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel
    }
    
}
