import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Contoh02c {

    public static void main(String[] args) {
        
        JFrame jf = new JFrame("My App");
        jf.setSize(300, 300);
        
        JLabel labelNIM = new JLabel("NIM");
        labelNIM.setFont(new Font("Calibri", Font.BOLD, 20));
        labelNIM.setBounds(20, 10, 50, 30);
        jf.add(labelNIM);

        jf.setLayout(null);
        jf.setVisible(true);
    }
    
}
