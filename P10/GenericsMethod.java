public class GenericsMethod {

    public static void main(String[] args) {
        Metode gen = new Metode();
        gen.<String>metodeGenerik("unhas");
        gen.<Integer>metodeGenerik(100);
    }
    
}

class Metode {

    public <T> void metodeGenerik(T data) {
        System.out.println("-- metode generik --");
        System.out.println(data);
    }

    // public void tanpaGenerik(int data) {
    //     System.out.println("-- bukan generik");
    //     System.out.println(data);
    // }

    // public void tanpaGenerik(double data) {
    //     System.out.println("-- bukan generik");
    //     System.out.println(data);
    // }

    // public void tanpaGenerik(String data) {
    //     System.out.println("-- bukan generik");
    //     System.out.println(data);
    // }

}
