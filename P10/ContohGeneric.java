public class ContohGeneric {

    public static void main(String[] args) {
        Data<Integer> bilangan = new Data<>(4);
        System.out.println(bilangan.getData());

        Data<String> kata = new Data<>("unhas");
        System.out.println(kata.getData());

        Data<Double> ipk = new Data<>(3.99);
        System.out.println(ipk.getData());
        
        Data<Float> pi  = new Data<>(3.1415f);
        System.out.println(pi.getData());
    }
    
}
